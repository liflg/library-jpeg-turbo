#!/bin/bash

set -e

select_multiarchname()
{
    echo "Please select the desired architecture:"
    echo "1): i386-linux-gnu"
    echo "2): x86_64-linux-gnu"
    echo "3): armeabi-linux-android"
    echo "4): armeabi_v7a-linux-android"
    read -r SELECTEDOPTION
    if [ x"$SELECTEDOPTION" = x"1" ]; then
        MULTIARCHNAME=i386-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"2" ]; then
        MULTIARCHNAME=x86_64-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"3" ]; then
        MULTIARCHNAME=armeabi-linux-android
    elif [ x"$SELECTEDOPTION" = x"4" ]; then
        MULTIARCHNAME=armeabi_v7a-linux-android
    else
        echo "Invalid option selected!"
        select_multiarchname
    fi
}

linux_build()
{
    mkdir "$BUILDDIR"
    rm -rf "$PREFIXDIR"

    ( cd "$BUILDDIR"
      cmake ../source/ \
         -DCMAKE_INSTALL_PREFIX="$PREFIXDIR" \
         -DCMAKE_BUILD_TYPE=Release \
         -DENABLE_STATIC=OFF \
         -DWITH_SIMD=OFF # FIXME: build error on buildserver, gcc version too old?
      make -j "$(nproc)" install
      rm -rf "${PREFIXDIR:?}"/{bin,lib/pkgconfig,share})
}

if [ -z "$MULTIARCHNAME" ]; then
    echo "\$MULTIARCHNAME is not set!"
    select_multiarchname
fi

if [ -z "$BUILDDIR" ]; then
    BUILDDIR="build_$MULTIARCHNAME"
fi

if [ -z "$PREFIXDIR" ]; then
    PREFIXDIR="$PWD/$MULTIARCHNAME"
fi

case "$MULTIARCHNAME" in
i386-linux-gnu)
    linux_build;;
x86_64-linux-gnu)
    linux_build;;
*)
    echo "$MULTIARCHNAME is not (yet) supported by this script."
    exit 1;;
esac

install -m664  source/LICENSE.md "$PREFIXDIR"/lib/LICENSE-jpeg-turbo.txt

rm -rf "$BUILDDIR"

echo "Jpeg-turbo for $MULTIARCHNAME is ready."
