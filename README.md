Website
=======
https://libjpeg-turbo.org/

License
=======
BSD-style (see the file source/LICENSE.md)

Version
=======
2.0.1

Source
======
libjpeg-turbo-2.0.1.tar.gz (sha256: e5f86cec31df1d39596e0cca619ab1b01f99025a27dafdfc97a30f3a12f866ff)

Required by
===========
* cegui04
* devil
* ogre10
